package com.github.jonduttweiler.apps.lab.polymer3.network.editor.panels;

import java.util.ArrayList;
import java.util.Arrays;

import com.github.jonduttweiler.vaadin.components.network.Network;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;

public class ClusteringPanel extends NetworkPanel{

    private String title = "Clustering panel";
    private static final long serialVersionUID = -1996504100437279059L;
    private TextArea results = new TextArea();
    private TextField idInput;
    private TextArea joinCondition;
    
    public ClusteringPanel(Network network){
        this.add(new H4(title));

        this.idInput = new TextField();
        idInput.setPlaceholder("id(s)");
        this.add(idInput);
    
        joinCondition = new TextArea();
        joinCondition.setLabel("Join condition:");
        this.add(joinCondition);
    

        Button clusterBtn = new Button("Cluster ids");
        clusterBtn.addClickListener(ev -> {
            ArrayList<String> idsNodes = new ArrayList<String>();
            idsNodes.addAll(Arrays.asList(idInput.getValue().split(",")));
            network.clusterNodes(idsNodes);
        });
        this.add(clusterBtn);
           

        Button clusterConditionBtn = new Button("Cluster Condition");
        clusterConditionBtn.addClickListener(ev -> {
            network.focus(idInput.getValue());
        });
        this.add(clusterConditionBtn);


        Button getNodesInCluster = new Button("get nodes in cluster");
        getNodesInCluster.addClickListener(ev -> {
            network.focus(idInput.getValue());
        });
        this.add(getNodesInCluster);


        Button unclusterBtn = new Button("Uncluster id");
        unclusterBtn.addClickListener(ev -> {
            network.focus(idInput.getValue());
        });
        this.add(unclusterBtn);


        idInput.getStyle().set("width","100%");
        joinCondition.getStyle().set("width","100%");
        clusterConditionBtn.getStyle().set("width","100%");
        clusterBtn.getStyle().set("width","100%");
        getNodesInCluster.getStyle().set("width","100%");
        unclusterBtn.getStyle().set("width","100%");

        results.getStyle().set("width","100%");
        this.add(results);
    }




}
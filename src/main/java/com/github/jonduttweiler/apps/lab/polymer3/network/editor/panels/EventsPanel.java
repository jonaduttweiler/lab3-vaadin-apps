package com.github.jonduttweiler.apps.lab.polymer3.network.editor.panels;

import java.util.function.Consumer;

import com.github.jonduttweiler.vaadin.components.network.Network;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H4;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;

import org.json.JSONObject;

import elemental.json.JsonObject;

public class EventsPanel extends NetworkPanel {

    private Network network;
    private boolean clickHandleEnable = false;
    private boolean dragHandleEnable = false;
    private Button toggleClickBtn;
    private Button toggleDragBtn;

    private TextArea results = new TextArea();
    private static final long serialVersionUID = -5858231606653565643L;

    public EventsPanel(Network network) {

        this.network = network;
        this.add(new H4("Events"));
        addFocusSection();
        addHandlerBtns();
        this.add(results);
    }

    private void addFocusSection() {

        TextField focusInput = new TextField();
        focusInput.getStyle().set("width", "40px");

        focusInput.addValueChangeListener(event -> {
            network.focus(focusInput.getValue());
        });

        Button focusBtn = new Button("Focus");
        focusBtn.addClickListener(event -> {
            network.focus(focusInput.getValue());
        });
        this.add(new HorizontalLayout(focusInput, focusBtn));
    }

    private void addHandlerBtns() {
        addClickControlSection();
        addDragControlSection();
    }


    private void addClickControlSection() {

        Consumer<JsonObject> clickHandler = ev -> {
            String stringifiedEvent = new JSONObject(ev.toJson()).toString(3);
            this.results.setValue(stringifiedEvent);
        };


        toggleClickBtn = new Button("OFF", event -> {
            clickHandleEnable = !clickHandleEnable;

            if (this.clickHandleEnable) {
                network.on("click", clickHandler);
            } else {
                network.off("click");
                this.results.setValue("");
            }
            toggleClickBtn.setText(clickHandleEnable ? "ON" : "OFF");
        });

        HorizontalLayout hl = new HorizontalLayout(new Text("Handle click: "), toggleClickBtn);
        hl.getStyle().set("padding","15px");
        this.add(hl);
    }




    private void addDragControlSection() {

        toggleDragBtn = new Button("OFF", event -> {
            dragHandleEnable = !dragHandleEnable;

            if (dragHandleEnable) {
                network.on("dragging", ev -> {
                    System.out.println(ev);
                    this.results.setValue(new JSONObject(ev.toJson()).toString(3));
                });

            } else {
                network.off("dragging");
                this.results.setValue("");
            }
            toggleDragBtn.setText(dragHandleEnable ? "ON" : "OFF");
        });


        HorizontalLayout hl =new HorizontalLayout(new Text("Handle drag: "), toggleDragBtn);
        hl.getStyle().set("padding","15px");
        this.add(hl);
    }

}
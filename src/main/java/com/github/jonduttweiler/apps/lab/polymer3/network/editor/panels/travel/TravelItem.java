package com.github.jonduttweiler.apps.lab.polymer3.network.editor.panels.travel;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.html.Div;

public class TravelItem extends Div{

    public TravelItem(String nodeId){
        this.getStyle().set("width","100%");
        this.add(new Text(nodeId));
        
    }
}
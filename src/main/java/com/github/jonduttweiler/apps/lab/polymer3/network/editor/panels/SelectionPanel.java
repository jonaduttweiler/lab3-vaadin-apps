package com.github.jonduttweiler.apps.lab.polymer3.network.editor.panels;

import java.util.ArrayList;
import java.util.Arrays;

import com.github.jonduttweiler.vaadin.components.network.Network;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;

public class SelectionPanel extends NetworkPanel{

    private String title = "Selection";
    private TextArea results = new TextArea();
    private static final long serialVersionUID = 7907968583225452435L;

    public SelectionPanel(Network network) {
        this.add(new H4(title));

        Button getSelectedNodesBtn = new Button("Get selected nodes");
        getSelectedNodesBtn.addClickListener(event -> {
            network.getSelectedNodes().thenAccept(nodesList -> {
                results.setLabel("Selected nodes");
                results.setValue(nodesList.toString());
                
            });
        });
        this.add(getSelectedNodesBtn);

        Button getSelectedEdgesBtn = new Button("Get selected edges");
        getSelectedEdgesBtn.addClickListener(event -> {
            network.getSelectedEdges().thenAccept( edgesList -> {
                results.setLabel("Selected edges");
                results.setValue(edgesList.toString());
            });
            
        });
        this.add(getSelectedEdgesBtn);

        
        Button setSelectionBtn = new Button("Set selection");
        TextField nodesIdsInput = new TextField();
        TextField edgesIdsInput = new TextField();

        nodesIdsInput.setPlaceholder("1,2,3,4");
        edgesIdsInput.setPlaceholder("e1,e2,e3,e4");
   
        this.add(nodesIdsInput);    
        this.add(edgesIdsInput);    

       
        setSelectionBtn.addClickListener(event -> {
            ArrayList<String> idsNodes = new ArrayList<String>();
            idsNodes.addAll(Arrays.asList(nodesIdsInput.getValue().split(",")));
    
            ArrayList<String> idsEdges = new ArrayList<String>();
            idsEdges.addAll(Arrays.asList(edgesIdsInput.getValue().split(",")));
       
            network.setSelection(idsNodes,idsEdges,false,false).thenAccept(nothing -> {
                results.setLabel("");
                results.setValue("Selection set");
            });
        });
        this.add(setSelectionBtn);


        Button unselectAllBtn = new Button("Unselect all");
      
        unselectAllBtn.addClickListener(event -> {
            network.unselectAll().thenAccept(nothing -> {
                results.setLabel("");
                results.setValue("");
            });
        });
        this.add(unselectAllBtn);

        this.add(results);




        nodesIdsInput.getStyle().set("width", "100%");
        edgesIdsInput.getStyle().set("width", "100%");
        getSelectedNodesBtn.getStyle().set("width", "100%");
        getSelectedEdgesBtn.getStyle().set("width", "100%");
        setSelectionBtn.getStyle().set("width", "100%");
        unselectAllBtn.getStyle().set("width", "100%");
        results.getStyle().set("width", "100%"); 
    }
}
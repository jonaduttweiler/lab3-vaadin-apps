
package com.github.jonduttweiler.apps.lab.polymer3.network.editor;

import com.github.jonduttweiler.vaadin.components.network.Network;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;

public class Toolbar extends Div{

    public Toolbar(Network network){
        this.setClassName("network-toolbar");

        //Button addNodeMode = new Button("add Node");
        //addNodeMode.addClickListener(event -> {
        //    network.addNodeMode();
        //});
        //this.add(addNodeMode);

        //Button editNode = new Button("edit Node");
        //editNode.addClickListener(event -> {
        //    network.editNode();
        //});

        Button deleteSelected = new Button(VaadinIcon.ERASER.create());
        deleteSelected.addClickListener(event -> {
            network.deleteSelected();
        });

        //Button export = new Button("Export");
        //export.addClickListener(event -> {
        //    network.export();
        //});
        
        Button addEdgeMode = new Button(VaadinIcon.CLUSTER.create());
        addEdgeMode.addClickListener(event -> {
            //network.addClassName("add-edge-active");
            network.addEdgeMode();

            network.once("release",ev -> {
              //  network.removeClassName("add-edge-active");
            });
        });

        

        //Button editEdgeMode = new Button("edit Edge");
        //editEdgeMode.addClickListener(event -> {
        //    network.editEdgeMode();
        //});
        //this.add(editEdgeMode);

       
        //addEdgeMode.addThemeVariants(ButtonVariant.LUMO_PRIMARY); //ButtonVariant.LUMO_SMALL
        //deleteSelected.addThemeVariants(ButtonVariant.LUMO_PRIMARY); //ButtonVariant.LUMO_SMALL
        //this.add(editNode);
        
        addEdgeMode.getElement().setProperty("title", "Agregar aristas");
        deleteSelected.getElement().setProperty("title", "Eliminar seleccionados");
        this.add(addEdgeMode);
        this.add(deleteSelected);

        
    }
}
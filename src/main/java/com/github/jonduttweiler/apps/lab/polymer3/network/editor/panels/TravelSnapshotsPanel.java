package com.github.jonduttweiler.apps.lab.polymer3.network.editor.panels;

import java.util.ArrayList;
import java.util.List;


import com.github.jonduttweiler.vaadin.components.network.Network;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.server.StreamRegistration;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinSession;

import org.json.JSONArray;
import org.json.JSONObject;
import java.text.DecimalFormat;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse.BodyHandlers;

import java.time.Instant;

public class TravelSnapshotsPanel extends NetworkPanel {

    private static final long serialVersionUID = 3485708452445882212L;
    private DecimalFormat df2 = new DecimalFormat("#.##");

    private Network network;
    private String title = "Travel Snapshots panel";
    private List<HorizontalLayout> travelView = new ArrayList<HorizontalLayout>();
    private List<JSONObject> travel = new ArrayList<JSONObject>();
    private VerticalLayout stepsContainer = new VerticalLayout();
    private int currentPos = -1;
    private Button prevBtn = new Button("◀", ev -> prev());
    private Button nextBtn = new Button("▶", ev -> next());

    public TravelSnapshotsPanel(Network network) {

        this.network = network;

        this.add(new H4(title));
        this.getStyle().set("width", "100%");
        stepsContainer.getStyle().set("width", "100%");
        this.addSnapshotBtns();
        this.addNavigationBtns();

        renderList();
        this.add(stepsContainer);
    }

    private void addSnapshotBtns() {

        Button takeSnapshotBtn = new Button("Take snapshot", ev -> addStep());
        takeSnapshotBtn.getStyle().set("width", "100%");
        this.add(takeSnapshotBtn);

        Button removeCurrentStep = new Button("Remove current step", ev -> removeStep(this.currentPos));
        removeCurrentStep.getStyle().set("width", "100%");
        this.add(removeCurrentStep);

        Button exportBtn = new Button("Export", ev -> exportTravel());
        exportBtn.getStyle().set("width", "100%");
        this.add(exportBtn);
    }

    private void addNavigationBtns() {

        prevBtn.addThemeVariants(ButtonVariant.LUMO_SMALL);
        nextBtn.addThemeVariants(ButtonVariant.LUMO_SMALL);
        prevBtn.getStyle().set("color", "white").set("background-color", "#000");
        nextBtn.getStyle().set("color", "white").set("background-color", "#000");

        HorizontalLayout btnContainers = new HorizontalLayout(prevBtn, nextBtn);
        btnContainers.getStyle().set("width", "100%").set("justify-content", "center");
        btnContainers.setAlignItems(Alignment.CENTER);

        this.add(btnContainers);

    }

    public int getCurrent() {
        return this.currentPos;
    }

    // move to prev element in travel.
    public int prev() {
        if (travel.size() == 0)
            return -1;

        this.currentPos = (this.currentPos == 0) ? (travel.size() - 1) : (this.currentPos - 1) % travel.size();
        setActiveStep(currentPos);
        return this.currentPos;
    }

    // move to next element in travel
    public int next() {
        if (travel.size() == 0)
            return -1;

        this.currentPos = (this.currentPos + 1) % travel.size();
        setActiveStep(currentPos);
        return this.currentPos;
    }

    private void setActiveStep(int pos) {
        if (pos < 0)
            return;

        JSONObject snapshot = travel.get(pos);

        JSONObject position = snapshot.getJSONObject("position");
        Double x = position.getDouble("x");
        Double y = position.getDouble("y");

        Double scale = snapshot.getDouble("scale");

        network.moveTo(x, y, scale, true);
        renderList();
    }

    private void renderList() {
        stepsContainer.removeAll();

        for (int idx = 0; idx < travelView.size(); idx++) {
            HorizontalLayout step = travelView.get(idx);

            if (idx == currentPos) {
                step.getElement().getClassList().add("active");
            } else {
                step.getElement().getClassList().remove("active");
            }
            stepsContainer.add(step);
        }
    }

    private void addStep() { // take snapshot

        this.network.getViewPositionFull(0.4f).thenAccept(snapshot -> {
            travel.add(snapshot);

            Div stepText = new Div(this.generateStepText(snapshot));
            Image stepImage = this.generateStepImage(snapshot);

            HorizontalLayout step = new HorizontalLayout(stepText, stepImage);
            step.addClassName("step-container"); // TODO: pass to another class
            travelView.add(step);
            renderList();
        });

    }

    private Text generateStepText(JSONObject snapshot) {
        try {
            JSONObject position = snapshot.getJSONObject("position");
            Double xDouble = position.getDouble("x");
            String x = df2.format(xDouble);
            Double yDouble = position.getDouble("y");
            String y = df2.format(yDouble);
            Double scaleDouble = snapshot.getDouble("scale");
            String scale = df2.format(scaleDouble);
            return new Text("{x:" + x + ", y:" + y + ", scale:" + scale + "}");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // return vaadin img
    private Image generateStepImage(JSONObject snapshot) {
        try {
            String imgSrc = snapshot.getString("img");
            Image img = new Image(imgSrc, "Snapshot");
            return img;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void removeStep(int position) {
        if (position >= 0 && position < this.travelView.size()) {
            this.travelView.remove(position);
            this.travel.remove(position);

            if (this.currentPos == this.travelView.size()) {
                this.currentPos -= 1;
            }
            setActiveStep(this.currentPos);

            renderList();
        }
    }

    //Esto es asincrono
    public void exportTravel() {
        this.network.export().thenAccept(network -> {
            try{
                InputStream generatedZip = getZip(network, new JSONArray(travel));
                sendStreamToUser(generatedZip);
            } catch(Exception e) {
                System.out.println(e.getMessage());
                //e.printStackTrace();
                String message = "Ha ocurrido un error al generar la exportación";
                new Notification(message, 3000).open();
            }

        });
    }

    private void sendStreamToUser(InputStream stream) {
        final StreamResource resource = new StreamResource(generateExportFilename(), () -> stream);
        final StreamRegistration registration = VaadinSession.getCurrent().getResourceRegistry()
                .registerResource(resource);
        UI.getCurrent().getPage().setLocation(registration.getResourceUri());
    }

    private InputStream getZip(JSONObject network, JSONArray travel) throws IOException, InterruptedException {

        String url = "http://localhost:3000/bundle"; // packer server

        JSONObject payload = new JSONObject().put("network", network).put("travel", travel);

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                            .header("Content-Type", "application/json")
                            .uri(URI.create(url))
                            .POST(BodyPublishers.ofString(payload.toString())).build();

        HttpResponse<InputStream> response = client.send(request, BodyHandlers.ofInputStream());

        if (response.statusCode() == 200) {
            return response.body();
        }
        return null;

    }

    private String generateExportFilename(){
        Instant instant = Instant.now();
        String ts = String.valueOf(instant.toEpochMilli());
        return "export-"+ts+".zip";
    }


    

}
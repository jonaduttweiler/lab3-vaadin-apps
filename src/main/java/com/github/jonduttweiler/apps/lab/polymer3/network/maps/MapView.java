package com.github.jonduttweiler.apps.lab.polymer3.network.maps;

import com.github.jonduttweiler.openlayers.OpenLayerMap;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("maps")
public class MapView extends VerticalLayout{

    public MapView(){
        this.getStyle().set("height","100vh");
        OpenLayerMap map = new OpenLayerMap(-60.628837,-32.946663,Double.valueOf(15));
        map.setEditionMode(true); 
        map.setMarkerMode(true); 
        this.add(map);
    }
}
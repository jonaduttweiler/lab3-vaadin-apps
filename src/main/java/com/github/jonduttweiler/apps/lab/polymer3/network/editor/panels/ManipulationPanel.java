package com.github.jonduttweiler.apps.lab.polymer3.network.editor.panels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.github.jonduttweiler.vaadin.components.network.Network;
import com.awesomecontrols.viswrapper.network.animations.EasingFunction;
import com.awesomecontrols.viswrapper.network.animations.MoveOptions;
import com.awesomecontrols.viswrapper.network.animations.NetworkAnimation;
import com.awesomecontrols.viswrapper.network.animations.NetworkPosition;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;

public class ManipulationPanel extends NetworkPanel {

    private String title = "Manipulation panel";
    private static final long serialVersionUID = -1996504100437279039L;
    private TextArea results = new TextArea();
    private TextField idInput;
    private TextField xInput;
    private TextField yInput;
    private TextField durationInput;
    private TextField scaleInput;

    public ManipulationPanel(Network network) {
        this.add(new H4(title));

        this.idInput = new TextField();
        idInput.setPlaceholder("id(s)");
        this.add(idInput);
        this.xInput = new TextField();
        xInput.setPlaceholder("x");
        this.add(xInput);
        this.yInput = new TextField();
        yInput.setPlaceholder("y");
        this.add(yInput);
        this.scaleInput = new TextField();
        scaleInput.setPlaceholder("Scale");
        this.add(scaleInput);
        this.durationInput = new TextField();
        durationInput.setPlaceholder("Duration");
        this.add(durationInput);

        Button fitBtn = new Button("Fit!");
        fitBtn.addClickListener(ev -> {
            List<String> idsNodes = new ArrayList<String>();
            idsNodes.addAll(Arrays.asList(idInput.getValue().split(",")));
            idsNodes = idsNodes.stream().filter(word -> !word.isEmpty()).collect(Collectors.toList());

            MoveOptions move = new MoveOptions().setAnimation(new NetworkAnimation(500, EasingFunction.easeOutCubic));

            if (idsNodes.size() > 0) {
                network.fit(idsNodes, move);
            } else {
                network.fit();
            }
        });
        this.add(fitBtn);

        Button focusBtn = new Button("Focus!");
        focusBtn.addClickListener(ev -> {
            network.focus(idInput.getValue());
        });
        this.add(focusBtn);

        Button moveToBtn = new Button("Move to!");
        moveToBtn.addClickListener(ev -> {
            Double x = Double.valueOf(xInput.getValue());
            Double y = Double.valueOf(yInput.getValue());
            Double scale = Double.valueOf(scaleInput.getValue());
            Double duration = Double.valueOf(durationInput.getValue());

            System.out.println("move to x:" + x + " y:" + y + " scale:" + scale + " duration:" + duration);

            MoveOptions move = new MoveOptions().setScale(scale).setPosition(new NetworkPosition(x, y))
                    .setAnimation(new NetworkAnimation(duration, EasingFunction.linear));

            network.moveTo(move);
            // network.moveTo(x,y,scale);
        });
        this.add(moveToBtn);

        Button getScaleBtn = new Button("get Scale");
        getScaleBtn.addClickListener(ev -> {
            network.getScale().thenAccept(scale -> {
                results.setLabel("scale:");
                results.setValue(scale.toString());
            });
        });
        this.add(getScaleBtn);

        Button getViewPosition = new Button("get View Position");
        getViewPosition.addClickListener(ev -> {
            network.getViewPosition().thenAccept(position -> {
                results.setLabel("view position:");
                results.setValue(position.toString(3));
            });
        });
        this.add(getViewPosition);

        Button exportBtn = new Button("Export");
        exportBtn.addClickListener(ev -> {
            network.export().thenAccept(jsonObject -> {
                results.setLabel("Export:");
                results.setValue(jsonObject.toString(3));
            });
        });
        this.add(exportBtn);

        idInput.getStyle().set("width", "100%");
        xInput.getStyle().set("width", "50%");
        yInput.getStyle().set("width", "50%");
        scaleInput.getStyle().set("width", "50%");
        durationInput.getStyle().set("width", "50%");
        fitBtn.getStyle().set("width", "100%");
        focusBtn.getStyle().set("width", "100%");
        exportBtn.getStyle().set("width", "100%");
        moveToBtn.getStyle().set("width", "100%");
        getScaleBtn.getStyle().set("width", "100%");
        getViewPosition.getStyle().set("width", "100%");

        results.getStyle().set("width", "100%");
        this.add(results);

    }

}
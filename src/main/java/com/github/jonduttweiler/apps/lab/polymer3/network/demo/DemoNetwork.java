package com.github.jonduttweiler.apps.lab.polymer3.network.demo;

import java.util.ArrayList;
import java.util.List;

import com.awesomecontrols.viswrapper.network.config.NetworkEdges;
import com.awesomecontrols.viswrapper.network.config.NetworkNodes;
import com.awesomecontrols.viswrapper.network.config.NetworkNodes.Icon;
import com.awesomecontrols.viswrapper.network.config.NetworkNodes.Shape;
import com.awesomecontrols.viswrapper.network.config.NetworkEdges.Smooth;
import com.awesomecontrols.viswrapper.network.config.NetworkEdges.Smooth.TYPE;

import com.github.jonduttweiler.vaadin.components.network.Network;

public class DemoNetwork extends Network {

    protected List<NetworkNodes> nodes;
    protected List<String> groupNames;

    public DemoNetwork() {
        nodes = new ArrayList<NetworkNodes>();
        groupNames = new ArrayList<String>();
        addGroups();
        addNodes();
        addEdges();
        
        setOptions();
    }

    protected void addNodes() {
    }

    protected void addEdges() {
    }

    protected void addGroups() {
        Icon prIcon = new Icon().setCode("\uf007").setColor("#EB534A");
        Icon victimaIcon = new Icon().setCode("\uf007").setColor("#15AABF");
        Icon intervinienteIcon = new Icon().setCode("\uf007").setColor("#AAAAAA");

        Icon iconCar = new Icon().setCode("\uf1b9").setColor("#FD7E14");
        Icon iconMotorcycle = new Icon().setCode("\uf21c").setColor("#82C91E");
        Icon iconPistas = new Icon().setCode("\uf54b").setColor("#856742");

        NetworkNodes groupPr = new NetworkNodes().setShape(Shape.icon).setIcon(prIcon);
        NetworkNodes groupVictima = new NetworkNodes().setShape(Shape.icon).setIcon(victimaIcon);
        NetworkNodes groupInterviniente = new NetworkNodes().setShape(Shape.icon).setIcon(intervinienteIcon);
        NetworkNodes groupCar = new NetworkNodes().setShape(Shape.icon).setIcon(iconCar);
        NetworkNodes groupMotorcycle = new NetworkNodes().setShape(Shape.icon).setIcon(iconMotorcycle);
        NetworkNodes groupPistas = new NetworkNodes().setShape(Shape.icon).setIcon(iconPistas);

        this.addGroup("presuntoResponsable", groupPr);
        this.addGroup("victima", groupVictima);
        this.addGroup("interviniente", groupInterviniente);
        this.addGroup("clue", groupPistas);
        this.addGroup("car", groupCar);
        this.addGroup("motorcycle", groupMotorcycle);

        groupNames.add("presuntoResponsable");
        groupNames.add("victima");
        groupNames.add("interviniente");
        groupNames.add("clue");
        groupNames.add("car");
        groupNames.add("motorcycle");

    }

    protected void setOptions() {
        Smooth smoothConf = new Smooth().setEnabled(true).setType(TYPE.dynamic);
        this.setEdgesOptions(new NetworkEdges().setLength(300).setSmooth(smoothConf));
    }


}
package com.github.jonduttweiler.apps.lab.polymer3.network.editor.Buttons;

import com.vaadin.flow.component.button.Button;

public class NodeAddButton extends Button {

    private static final long serialVersionUID = -4608090522566208700L;


    String nodeId;
    String nodeGroup;


    public NodeAddButton(String label, String id, String group){
        super(label);
        this.nodeId = id;
        this.nodeGroup = group;
    }

    public String getNodeId() {
		return this.nodeId;
	}


	public String getNodeGroup() {
		return this.nodeGroup;
	}


    
}
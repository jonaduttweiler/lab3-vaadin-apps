package com.github.jonduttweiler.apps.lab.polymer3.network;

import com.github.jonduttweiler.vaadin.components.network.Network;
import com.github.jonduttweiler.apps.lab.polymer3.network.demo.DemoNetwork;
import com.github.jonduttweiler.apps.lab.polymer3.network.demo.DemoStaticNetwork;
import com.github.jonduttweiler.apps.lab.polymer3.network.demo.DemoDynamicNetwork;
import com.github.jonduttweiler.apps.lab.polymer3.network.editor.NetworkEditor;
import com.github.jonduttweiler.apps.lab.polymer3.network.editor.Toolbar;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.Route;

/**
 * The main view contains a button and a click listener.
 */
@Route("network")
@StyleSheet("./styles/network-view.css")
public class NetworkView extends HorizontalLayout {

    private static final long serialVersionUID = 1L;
    
    private Network network;

    public NetworkView() {
        this.addClassName("vis-view");
        network = new DemoDynamicNetwork(); 
        
        Div networkContainer = new Div();
        Div toolbar = new Toolbar(network); 

        networkContainer.addClassName("network-container");
        networkContainer.add(toolbar, network);
        this.add(networkContainer);

        NetworkEditor editorPanel = new NetworkEditor(network);
        editorPanel.setClassName("network-editor");
        this.add(editorPanel);

        network.stabilize();

    }


}

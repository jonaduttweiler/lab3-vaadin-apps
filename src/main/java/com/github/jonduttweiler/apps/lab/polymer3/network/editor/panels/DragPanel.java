package com.github.jonduttweiler.apps.lab.polymer3.network.editor.panels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.github.jonduttweiler.vaadin.components.network.Network;
import com.awesomecontrols.viswrapper.network.config.NetworkEdges;
import com.awesomecontrols.viswrapper.network.config.NetworkInteraction;
import com.awesomecontrols.viswrapper.network.config.NetworkNodes;
import com.awesomecontrols.viswrapper.network.config.NetworkPhysics;
import com.awesomecontrols.viswrapper.network.config.NetworkNodes.Shape;
import com.github.jonduttweiler.apps.lab.polymer3.network.editor.panels.subpanels.*;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.html.Div;

import com.awesomecontrols.viswrapper.network.config.NetworkNodes.Icon;

public class DragPanel extends NetworkPanel {

    private Accordion accordion;

    public DragPanel(Network network) {
        this.accordion = new Accordion();

        List<List<String>> intervinientes = new ArrayList<>();
        intervinientes.add(new ArrayList<>(Arrays.asList("VILLARRUEL, VICTOR HUGO", "5", "victima")));
        intervinientes.add(new ArrayList<>(Arrays.asList("PERITO", "6", "interviniente")));
        intervinientes.add(new ArrayList<>(Arrays.asList("SOSA, FELIX (En Proceso)", "1", "presuntoResponsable")));
        intervinientes.add(new ArrayList<>(Arrays.asList("SOSA, GUSTAVO (En Proceso)", "2", "presuntoResponsable")));
        intervinientes.add(new ArrayList<>(Arrays.asList("SOSA, MILENA (En Proceso)", "3", "presuntoResponsable")));
        intervinientes.add(new ArrayList<>(Arrays.asList("SOSA, MILTON (En Proceso)", "4", "presuntoResponsable")));
        intervinientes.add(new ArrayList<>(Arrays.asList("BERTI, MELANI (En Proceso)", "7", "presuntoResponsable")));
        intervinientes.add(new ArrayList<>(Arrays.asList("ESCOBAR, CLARA (En Proceso)", "8", "presuntoResponsable")));
        intervinientes.add(new ArrayList<>(Arrays.asList("GONZALEZ, ROXANA (En Proceso)", "9", "presuntoResponsable")));
        
           
        Div intervinientesPanel = new PanelNodesDrag(intervinientes);
        
        List<List<String>> elementos = new ArrayList<>();
        elementos.add(new ArrayList<>(Arrays.asList("Automovil AB 123 CD","10","car")));
        elementos.add(new ArrayList<>(Arrays.asList("Moto A12 3BCD","11","motorcycle")));
        elementos.add(new ArrayList<>(Arrays.asList("Moto A12 4BCD","12","motorcycle")));
        elementos.add(new ArrayList<>(Arrays.asList("Rastros","13","clue")));
    
        Div elementosPanel = new PanelNodes(network, elementos);

        PanelEdicion panelEdicion = new PanelEdicion(network);

        accordion.add("Intervinientes",intervinientesPanel );
        accordion.add("Elementos", elementosPanel);
        accordion.add("Detalles",panelEdicion); //we can implement publish subscriber pattern?


        this.add(accordion);
        this.setNetworkOptions(network);

        panelEdicion.setAccordion(accordion);
    }

    private void setNetworkOptions(Network network) {

        Icon personIcon = new Icon().setCode("\uf007").setColor("#15AABF");
        Icon prIcon = new Icon().setCode("\uf007").setColor("#EB534A");
        Icon victimaIcon = new Icon().setCode("\uf007").setColor("#15AABF");
        Icon intervinienteIcon = new Icon().setCode("\uf007").setColor("#AAAAAA");
        
        Icon iconCar = new Icon().setCode("\uf1b9").setColor("#FD7E14");
        Icon iconMotorcycle = new Icon().setCode("\uf21c").setColor("#82C91E");
        Icon iconPistas = new Icon().setCode("\uf54b").setColor("#856742");

        NetworkNodes groupPr = new NetworkNodes().setShape(Shape.icon).setIcon(prIcon);
        NetworkNodes groupVictima = new NetworkNodes().setShape(Shape.icon).setIcon(victimaIcon);
        NetworkNodes groupInterviniente = new NetworkNodes().setShape(Shape.icon).setIcon(intervinienteIcon);
        NetworkNodes groupCar = new NetworkNodes().setShape(Shape.icon).setIcon(iconCar);
        NetworkNodes groupMotorcycle = new NetworkNodes().setShape(Shape.icon).setIcon(iconMotorcycle);
        NetworkNodes groupPistas = new NetworkNodes().setShape(Shape.icon).setIcon(iconPistas);
        


        network.addGroup("presuntoResponsable", groupPr);
        network.addGroup("victima", groupVictima);
        network.addGroup("interviniente", groupInterviniente);
        network.addGroup("clue", groupPistas);
        network.addGroup("car", groupCar);
        network.addGroup("motorcycle", groupMotorcycle);

        network.setPhysicsOptions(new NetworkPhysics().setEnabled(false));
        network.setInteractionOptions(new NetworkInteraction()
                .setMultiselect(true)
                .setKeyboard(new NetworkInteraction.Keyboard().setEnabled(true)));
        network.setEdgesOptions(new NetworkEdges().setSmooth(new NetworkEdges.Smooth().setEnabled(false)));
    }

   
 
}
package com.github.jonduttweiler.apps.lab.polymer3.network.forms;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.dnd.DragSource;
import com.vaadin.flow.component.html.Div;

/**
 * 
 * Starting a drag from a component that has contents inside shadow dom does not
 * work in Firefox due to https://bugzilla.mozilla.org/show_bug.cgi?id=1521471.
 * Thus currently Vaadin components like TextField, DatePicker and ComboBox
 * cannot be dragged by the user in Firefox.</em>
 * 
 */
public class DraggableElement extends Div {

    private static final long serialVersionUID = 2348395078859494443L;
    private DragSource<Div> dragSource;

    public DraggableElement(String label, String data) {

        this.addClassName("draggable-button");
        this.add(new Text(label));

        dragSource = DragSource.create(this);
        dragSource.setDragData(data);
    }

    public void setDragData(String data) {
        dragSource.setDragData(data);
    }

}
package com.github.jonduttweiler.apps.lab.polymer3.network.editor;

import java.util.ArrayList;
import java.util.List;

import com.github.jonduttweiler.vaadin.components.network.Network;
import com.github.jonduttweiler.apps.lab.polymer3.network.editor.panels.*;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class NetworkEditor extends VerticalLayout {

    private static final long serialVersionUID = 241444310444380760L;
    private List<NetworkPanel> panels = new ArrayList<NetworkPanel>();
    private Div panelContainer;
    private int idxCurrentPanel = 3 ;
    


    public NetworkEditor(Network network) {
        //add prev and next btns
        Button prevBtn = new Button("◀");
        Button nextBtn = new Button("▶");

        prevBtn.addClickListener(ev -> prevPanel());
        nextBtn.addClickListener(ev -> nextPanel());

        prevBtn.addThemeVariants(ButtonVariant.LUMO_SMALL);
        nextBtn.addThemeVariants(ButtonVariant.LUMO_SMALL);


        HorizontalLayout btnContainers = new HorizontalLayout(prevBtn,nextBtn);
        btnContainers.addClassName("buttons-container");
        btnContainers.setAlignItems(Alignment.CENTER);

        this.add(btnContainers);

        panels.add(new DragPanel(network));
        panels.add(new NodeCRUDPanel(network));
        panels.add(new EdgeCRUDPanel(network));
        panels.add(new TravelSnapshotsPanel(network));
        panels.add(new ManipulationPanel(network));
        panels.add(new ClusteringPanel(network));
        panels.add(new InformationPanel(network));
        panels.add(new EventsPanel(network));
        panels.add(new SelectionPanel(network));

        panelContainer = new Div();
        panelContainer.getStyle().set("width","100%").set("margin-top", "0px");
        panelContainer.add(panels.get(idxCurrentPanel));
        this.add(panelContainer);
    
    }


    private void nextPanel(){
        this.idxCurrentPanel = (this.idxCurrentPanel + 1) % panels.size();
        panelContainer.removeAll();
        panelContainer.add(panels.get(idxCurrentPanel));
    }
    private void prevPanel(){
        this.idxCurrentPanel =  (this.idxCurrentPanel == 0) ? (panels.size() - 1) : (this.idxCurrentPanel - 1) % panels.size();
        panelContainer.removeAll();
        panelContainer.add(panels.get(idxCurrentPanel));
    }






}
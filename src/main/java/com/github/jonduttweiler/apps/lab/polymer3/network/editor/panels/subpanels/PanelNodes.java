package com.github.jonduttweiler.apps.lab.polymer3.network.editor.panels.subpanels;

import java.util.List;

import com.github.jonduttweiler.vaadin.components.network.Network;
import com.awesomecontrols.viswrapper.network.config.NetworkNodes;
import com.github.jonduttweiler.apps.lab.polymer3.network.forms.DraggableElement;
import com.github.jonduttweiler.apps.lab.polymer3.network.editor.Buttons.NodeAddButton;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dnd.DragSource;
import com.vaadin.flow.component.dnd.DragStartEvent;
import com.vaadin.flow.component.html.Div;

import org.json.JSONObject;

public class PanelNodes extends Div {
    ComponentEventListener<ClickEvent<Button>> addNode;

    public PanelNodes(final Network network, final List<List<String>> elementsToAdd) {
        initListener(network);
        attachListeners(elementsToAdd);
       
    }

    private void initListener(final Network network) {
        this.addNode = ev -> {
            final NodeAddButton source = (NodeAddButton) ev.getSource();
            final String id = source.getNodeId();
            final String label = source.getText();
            final String group = source.getNodeGroup();
            final NetworkNodes node = new NetworkNodes().setId(id).setLabel(label).setGroup(group);

            //network.addClassName("add-active");

            network.once("click", evclick -> {
                Double x = evclick.getObject("pointer").getObject("canvas").getNumber("x");
                Double y = evclick.getObject("pointer").getObject("canvas").getNumber("y");
                node.setX(x);
                node.setY(y);
                network.addNode(node).thenApply(result -> {
                    if(result){
                        System.out.println("Node added!");
                    }
                    
                    return result;
                });
                //network.removeClassName("add-active");
            });

        };
    }

    private void attachListeners(final List<List<String>> elementsToAdd ){
        elementsToAdd.forEach(nodeToAdd -> {
            final String label = nodeToAdd.get(0);
            final String id = nodeToAdd.get(1);
            final String group = nodeToAdd.get(2);

            final String dragData = new JSONObject()
                                .put("label",label)
                                .put("id",id)
                                .put("group",group)
                                .toString();

            final DraggableElement draggable = new DraggableElement(label,dragData);
            this.add(draggable);       
        });
    }


}

package com.github.jonduttweiler.apps.lab.polymer3.network.editor.panels.subpanels;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;


import com.github.jonduttweiler.vaadin.components.network.Network;
import com.github.jonduttweiler.apps.lab.polymer3.network.forms.DynamicForm;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import elemental.json.JsonObject;

public class PanelEdicion extends VerticalLayout {

    private Accordion accordion;


    public PanelEdicion(Network network) {

        this.getStyle().set("padding", "10px 0px");
        VerticalLayout containerDynamicForm = new VerticalLayout();

        network.on("click", ev -> {
            addDynamicForm(network, ev, containerDynamicForm);
        });

        this.add(containerDynamicForm);

    }

    private void addDynamicForm(Network network, JsonObject ev, VerticalLayout container) {
        JSONObject eventInfo = new JSONObject(ev.toJson());

        JSONArray nodes = eventInfo.getJSONArray("nodes");
        JSONArray edges = eventInfo.getJSONArray("edges");

        if(nodes.length() > 0  || edges.length() > 0 ) setPanelAsActive();

        if (nodes.length() > 0) {
            String nodeId = nodes.getString(0);
            network.getNode(nodeId).thenAccept(nodeInfo -> addNodeForm(nodeInfo, network, container));
        } else if (edges.length() > 0) {

            String edgeId = edges.getString(0);
            network.getEdge(edgeId).thenAccept(edgeInfo -> addEdgeForm(edgeInfo, network, container));
        }

        return;
    }

    private <T extends HasComponents> void addNodeForm(JSONObject edgeInfo, Network network, T container) {
        List<String> hidden = Arrays.asList("id", "x", "y");
        List<String> newFields = Arrays.asList("label","color","size","shape","group");

        DynamicForm form = new DynamicForm(edgeInfo, hidden, newFields, network,"node");
        container.removeAll();
        container.add(form);

    }

    private <T extends HasComponents> void addEdgeForm(JSONObject edgeInfo, Network network, T container) {
        List<String> hidden = Arrays.asList("id", "from", "to");
        List<String> newFields = Arrays.asList("label", "color", "width", "arrows", "title");

        DynamicForm form = new DynamicForm(edgeInfo, hidden, newFields, network,"edge");
        container.removeAll();
        container.add(form);
    }


    private void setPanelAsActive(){
        Optional.ofNullable(this.accordion).ifPresent(accordion -> accordion.open(2));
        return;
    }


    public void setAccordion(Accordion accordion){
        this.accordion = accordion;
        return;
    }

}

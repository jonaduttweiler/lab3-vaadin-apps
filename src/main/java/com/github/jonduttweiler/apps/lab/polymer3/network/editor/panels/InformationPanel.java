package com.github.jonduttweiler.apps.lab.polymer3.network.editor.panels;

import java.util.ArrayList;
import java.util.Arrays;


import com.github.jonduttweiler.vaadin.components.network.Network;
import com.awesomecontrols.viswrapper.network.config.NetworkEdges;
import com.awesomecontrols.viswrapper.network.config.NetworkLayout;
import com.awesomecontrols.viswrapper.network.config.NetworkPhysics;
import com.awesomecontrols.viswrapper.network.config.NetworkEdges.Smooth;
import com.awesomecontrols.viswrapper.network.config.NetworkLayout.Hierarchical;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;

public class InformationPanel extends NetworkPanel {

    private String title = "Information panel";
    private static final long serialVersionUID = -1996504100437279038L;
    private TextArea results = new TextArea();
    private TextField idInput;
    private TextField xInput;
    private TextField yInput;
    private boolean physicsOn = true;
    private boolean smoothEdges = true;
    private boolean hierarchical = false;

    public InformationPanel(Network network) {
        this.add(new H4(title));

        this.idInput = new TextField();
        idInput.setPlaceholder("id(s)");
        this.add(idInput);
        this.xInput = new TextField();
        xInput.setPlaceholder("x");
        this.add(xInput);
        this.yInput = new TextField();
        yInput.setPlaceholder("y");
        this.add(yInput);

        
        Button togglePhysicsBtn = new Button("tooglePhysics");
        togglePhysicsBtn.addClickListener(ev -> {
            physicsOn = !physicsOn;            
            //network setPhysicsOptions... 
            network.setPhysicsOptions(new NetworkPhysics().setEnabled(physicsOn)); 
        });
        this.add(togglePhysicsBtn);


        //Agreguemos opciones al smooth edges!
        Button setSmoothEdgesBtn = new Button("smoothEdges");
        setSmoothEdgesBtn.addClickListener(ev -> {
            smoothEdges = !smoothEdges;            
            
            Smooth smooth1 = new Smooth().setEnabled(true);
            Smooth smooth2 = new Smooth().setEnabled(true).setType(Smooth.TYPE.continuous);

            if(smoothEdges){
                network.setEdgesOptions(new NetworkEdges().setSmooth(smooth1)); 
            } else {
                network.setEdgesOptions(new NetworkEdges().setSmooth(smooth2)); 
            }
        });
        this.add(setSmoothEdgesBtn);


        
        Button setHierarchicalBtn = new Button("set Hierarchical");
        setHierarchicalBtn.addClickListener(ev -> {
            hierarchical = !hierarchical;
            network.setLayoutOptions(new NetworkLayout().setHierarchical(new Hierarchical().setEnabled(hierarchical))); 
        });
        this.add(setHierarchicalBtn);
        

        Button exportBtn = new Button("Export");
        exportBtn.addClickListener(ev -> {
            network.export().thenAccept(jsonObject -> {
                results.setLabel("Export:");
                results.setValue(jsonObject.toString(3));
            });
        });
        this.add(exportBtn);





        Button getPositionsBtn = new Button("getPositions");
        getPositionsBtn.addClickListener(ev -> {
            ArrayList <String> ids  = new ArrayList<String>(); 
            ids.addAll(Arrays.asList(idInput.getValue().split(",")));

            network.getPositions(ids).thenAccept(positions -> {
                clearPositionFields();
                results.setLabel("Positions:");
                results.setValue(positions.toString(3));
            });
        });

        this.add(getPositionsBtn);

        Button moveNodeBtn = new Button("moveNode");
        moveNodeBtn.addClickListener(ev -> {
            String id = idInput.getValue();
            int x = Integer.valueOf(xInput.getValue());
            int y = Integer.valueOf(yInput.getValue());
            network.moveNode(id, x, y);
        });
        this.add(moveNodeBtn);

        Button getBoundingBoxBtn = new Button("getBoundingBox");
        getBoundingBoxBtn.addClickListener(ev -> {
            clearPositionFields();
            String id = idInput.getValue();
            network.getBoundingBox(id).thenAccept(result -> {
                System.out.println(result);
                results.setLabel("Bounding box:");
                results.setValue(result.toString(3));
            });

        });
        this.add(getBoundingBoxBtn);

        Button getConnectedNodesBtn = new Button("getConnectedNodes");
        getConnectedNodesBtn.addClickListener(ev -> {
            clearPositionFields();
            String id = idInput.getValue();
            network.getConnectedNodes(id, "").thenAccept(result -> {/* to or from */
                System.out.println(result);
                results.setLabel("Connected nodes:");
                results.setValue(result.toString());
            });

        });
        this.add(getConnectedNodesBtn);

        Button getConnectedEdgesBtn = new Button("getConnectedEdges");
        getConnectedEdgesBtn.addClickListener(ev -> {
            clearPositionFields();
            String id = idInput.getValue();
            network.getConnectedEdges(id).thenAccept(result -> {/* to or from */
                System.out.println(result);
                results.setLabel("Connected edges:");
                results.setValue(result.toString());
            });

        });
        this.add(getConnectedEdgesBtn);

        
        idInput.getStyle().set("width","100%");
        xInput.getStyle().set("width","100%");
        yInput.getStyle().set("width","100%");
        togglePhysicsBtn.getStyle().set("width","100%");
        exportBtn.getStyle().set("width","100%");
        setSmoothEdgesBtn.getStyle().set("width","100%");
        setHierarchicalBtn.getStyle().set("width","100%");
        getPositionsBtn.getStyle().set("width","100%");
        moveNodeBtn.getStyle().set("width","100%");
        getBoundingBoxBtn.getStyle().set("width","100%");
        getConnectedNodesBtn.getStyle().set("width","100%");
        getConnectedEdgesBtn.getStyle().set("width","100%");
        results.getStyle().set("width","100%");

        this.add(results);

    }


    private void clearPositionFields(){
        this.xInput.setValue("");
        this.yInput.setValue("");
    }


}
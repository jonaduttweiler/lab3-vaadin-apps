package com.github.jonduttweiler.apps.lab.polymer3.network.editor.panels;

import com.github.jonduttweiler.vaadin.components.network.Network;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.textfield.TextField;

public class EdgeCRUDPanel extends NetworkPanel {

    private String title = "Edge CRUD";
    private static final long serialVersionUID = 1L;

    public EdgeCRUDPanel(Network network) {
        this.add(new H4(title));

        TextField edgeIdInput = new TextField("Edge id");
        edgeIdInput.getStyle().set("width", "100%");
        edgeIdInput.setPlaceholder("exx");
        this.add(edgeIdInput);

        TextField fromInput = new TextField("From");
        fromInput.getStyle().set("width", "100%");
        this.add(fromInput);

        TextField toInput = new TextField("To");
        toInput.getStyle().set("width", "100%");
        this.add(toInput);

        Button addEdgeBtn = new Button("Add/update");
        addEdgeBtn.addClickListener(event -> {
            String edgeId = edgeIdInput.getValue();
            String from = fromInput.getValue();
            String to = toInput.getValue();

            network.updateEdge(edgeId, from, to).thenAccept(System.out::println);
        });

        this.add(addEdgeBtn);


        Button removeBtn = new Button("Remove");
        removeBtn.addClickListener(event -> {
            String id = edgeIdInput.getValue();
            network.removeEdge(id).thenAccept(System.out::println);
        });

        this.add(removeBtn);

    }
}
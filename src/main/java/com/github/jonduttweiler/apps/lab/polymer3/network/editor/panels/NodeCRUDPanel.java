package com.github.jonduttweiler.apps.lab.polymer3.network.editor.panels;

import com.github.jonduttweiler.vaadin.components.network.Network;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.textfield.TextField;

public class NodeCRUDPanel extends NetworkPanel{

    private String title = "Node CRUD";
    private static final long serialVersionUID = 1L;
    
    public NodeCRUDPanel(Network network){
        this.add(new H4(title));

        TextField nodeIdInput = new TextField("Node id");
        nodeIdInput.getStyle().set("width", "100%");
        this.add(nodeIdInput);

        TextField nodeLabel = new TextField("label");
        nodeLabel.getStyle().set("width", "100%");
        this.add(nodeLabel);

        TextField nodeGroup = new TextField("Group");
        nodeGroup.getStyle().set("width", "100%");
        this.add(nodeGroup);

        Button addNodeBtn = new Button("Add/update");
        addNodeBtn.addClickListener(event -> {
            String nodeId = nodeIdInput.getValue();
            String label = nodeLabel.getValue();
            String group = nodeGroup.getValue();

            network.updateNode(nodeId,label,group).thenAccept(System.out::println);
        });

        this.add(addNodeBtn);

    
        Button removeBtn = new Button("Remove");
        removeBtn.addClickListener(event -> {
            String nodeId = nodeIdInput.getValue();
            network.removeNode(nodeId).thenAccept(System.out::println);
        });

        this.add(removeBtn);
    }


}
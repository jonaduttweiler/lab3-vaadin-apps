package com.github.jonduttweiler.apps.lab.polymer3.network.editor.panels.subpanels;

import java.util.List;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dnd.DragSource;
import com.vaadin.flow.component.html.Div;

import org.json.JSONObject;

public class PanelNodesDrag extends Div {
    ComponentEventListener<ClickEvent<Button>> addNode;

    public PanelNodesDrag(List<List<String>> elementsToAdd) {
        attachListeners(elementsToAdd);
    }

    private void attachListeners(List<List<String>> elementsToAdd ){
        elementsToAdd.forEach(nodeToAdd -> {
            String label = nodeToAdd.get(0);
            String id = nodeToAdd.get(1);
            String group = nodeToAdd.get(2);

            addDraggableElement(id,label,group);
        });
    }
    

    private void addDraggableElement(String id, String label, String group){
        Div elementDraggable = new Div(new Text(label));
        elementDraggable.addClassName("draggable-button");
        this.add(elementDraggable);

        //Add drag and drop support
        DragSource<Div> dragSource = DragSource.create(elementDraggable);
        JSONObject data = new JSONObject()
                            .put("id",id)
                            .put("label",label)
                            .put("group",group);

        dragSource.setDragData(data.toString());
    };

    

}

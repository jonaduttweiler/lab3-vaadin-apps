package com.github.jonduttweiler.apps.lab.polymer3.network.demo;

import com.awesomecontrols.viswrapper.network.config.NetworkEdges;
import com.awesomecontrols.viswrapper.network.config.NetworkNodes;


public class DemoStaticNetwork extends DemoNetwork {

    public DemoStaticNetwork(){
        super();
    }

    protected void addNodes() {

        this.addNode(new NetworkNodes("7ly80g", "7ly80g", "presuntoResponsable"));
        this.addNode(new NetworkNodes("hx6uqd", "hx6uqd", "victima"));
        this.addNode(new NetworkNodes("x2lr5r", "x2lr5r", "interviniente"));
        this.addNode(new NetworkNodes("4c4wki", "4c4wki", "clue"));
        this.addNode(new NetworkNodes("kke7dl", "kke7dl", "car"));
        this.addNode(new NetworkNodes("a1gcpa", "a1gcpa", "motorcycle"));
    }

    protected void addEdges() {

        this.addEdge(new NetworkEdges().setFrom("7ly80g").setTo("hx6uqd").setColor("#FF6440").setWidth(3));
        this.addEdge(new NetworkEdges().setFrom("x2lr5r").setTo("kke7dl").setColor("#B5E2E8").setWidth(4));
        this.addEdge(new NetworkEdges().setFrom("kke7dl").setTo("hx6uqd").setColor("#B5E2E8").setWidth(4));
        this.addEdge(new NetworkEdges().setFrom("kke7dl").setTo("a1gcpa").setColor("#99C47E").setWidth(5));
        this.addEdge(new NetworkEdges().setFrom("4c4wki").setTo("7ly80g").setColor("#32C4EB").setWidth(3));
    }


}
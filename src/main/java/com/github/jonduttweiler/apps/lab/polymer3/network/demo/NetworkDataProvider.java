package com.github.jonduttweiler.apps.lab.polymer3.network.demo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import com.awesomecontrols.viswrapper.network.config.NetworkEdges;
import com.awesomecontrols.viswrapper.network.config.NetworkGroup;
import com.awesomecontrols.viswrapper.network.config.NetworkNodes;
import com.awesomecontrols.viswrapper.network.config.NetworkNodes.Color;
import com.awesomecontrols.viswrapper.network.config.NetworkNodes.Icon;
import com.awesomecontrols.viswrapper.network.config.NetworkNodes.Shape;

import org.json.JSONObject;


public class NetworkDataProvider {

        public ArrayList<NetworkNodes> nodesArray;
        public ArrayList<NetworkEdges> edgesArray;
        public NetworkGroup groups;

        public NetworkDataProvider() {
                initNodes();
                initEdges();
                initGroups();

        }

        private void initNodes() {
                this.nodesArray = new ArrayList<>(
                                Arrays.asList(new NetworkNodes("1", "Node 1", "alpha").setTitle("Node 1"),
                                                new NetworkNodes("2", "Node 2", "alpha").setTitle("Node 2"),
                                                new NetworkNodes("3", "Node 3", "alpha").setTitle("Node 3"),
                                                new NetworkNodes("4", "Node 4", "alpha").setTitle("Node 4"),
                                                new NetworkNodes("5", "Node 5", "beta").setTitle("Node 5"),
                                                new NetworkNodes("6", "Node 6", "beta").setTitle("Node 6"),
                                                new NetworkNodes("7", "Node 7", "beta").setTitle("Node 7"),
                                                new NetworkNodes("8", "Node 8", "gamma").setTitle("Node 8"),
                                                new NetworkNodes("9", "Node 9", "gamma").setTitle("Node 9"),
                                                new NetworkNodes("10", "Node 10", "delta").setTitle("Node 10"),
                                                new NetworkNodes("11", "Node 11", "eta").setTitle("Node 11"),
                                                new NetworkNodes("12", "Node 12", "epsilon").setTitle("Node 12"),
                                                new NetworkNodes("13", "Node 13", "dseta").setTitle("Node 13"),
                                                new NetworkNodes("14", "Node 14", "eta").setTitle("Node 14"),
                                                new NetworkNodes("15", "Node 15").setTitle("Node 15")
                                                                .setShape(Shape.image)
                                                                .setImage("https://picsum.photos/100/100")));

        }

        private void initEdges() {
                this.edgesArray = new ArrayList<>();

                edgesArray.add(new NetworkEdges("e1", "1", "6"));
                edgesArray.add(new NetworkEdges("e4", "2", "5"));
                edgesArray.add(new NetworkEdges("e5", "4", "7"));
                edgesArray.add(new NetworkEdges("e7", "3", "5"));
                edgesArray.add(new NetworkEdges("e8", "2", "7"));
                edgesArray.add(new NetworkEdges("e9", "5", "11"));
                edgesArray.add(new NetworkEdges("e9_15", "5", "15"));

                edgesArray.add(new NetworkEdges("e11", "1", "10"));

                edgesArray.add(new NetworkEdges("e_3_12", "3", "12").setColor("#0D63FF").setWidth(2).setDashes(true));
                edgesArray.add(new NetworkEdges("e_3_13", "3", "13").setColor("#0DFF94").setWidth(3));
                edgesArray.add(new NetworkEdges("e_3_14", "3", "14").setColor("#0C94E8").setWidth(4));

                edgesArray.add(new NetworkEdges("e2", "1", "4").setColor("#E82A07").setWidth(5));

                edgesArray.add(new NetworkEdges("e10", "9", "4").setColor("#4B65FF")
                                .setArrows(new NetworkEdges.Arrows().setTo(true)).setWidth(2).setDashes(true));
                edgesArray.add(new NetworkEdges("e6", "1", "8").setColor("#9D0CE8").setArrows("from").setWidth(2)
                                .setDashes(true));

                edgesArray.add(new NetworkEdges("e3", "2", "4", "e3").setDashes(true).setColor("#0CE85C")
                                .setArrows("to").setWidth(2).setLength(250));

                edgesArray.add(new NetworkEdges("ec3", "2", "4", "e3").setDashes(true).setColor("#0CE85C")
                                .setArrows("from;to").setWidth(2).setLength(250));

        }

        private void initGroups() {
                this.groups = new NetworkGroup();
                Random random = new Random();
                String imgDimensions = "/250/250";

                NetworkNodes alpha = new NetworkNodes().setShape(Shape.icon)
                                .setIcon(new Icon().setCode("\uf2be").setColor("#3299FF")).setSize(30);

                NetworkNodes beta = new NetworkNodes().setShape(Shape.diamond).setSize(15)
                                .setColor(new Color().setBorder("#12801B").setBackground("#70FF7C")
                                                .setHighlight(new NetworkNodes.Color.Highlight().setBorder("#12803C")
                                                                .setBackground("#24FF78")));

                /*
                 * 
                 * TODO: crear un test con el grupo beta y comparar que el metodo toString
                 * devuelva lo siguiente:
                 * 
                 * groups.addGroup("beta: {"+ "    shape: 'diamond',"+ "        size: 15,"+
                 * "          color: {"+ "           border: '#12801B',"+
                 * "            background: '#70FF7C',"+ "            highlight: {"+
                 * "                border: '#12803C',"+
                 * "                background: '#24FF78'"+ "        }"+ "    }"+ "}");
                 */

                /*
                 * NetworkNodes gamma = new NetworkNodes().setShape(Shape.triangle).setSize(15)
                 * .setColor(new
                 * Color().setBorder("#CC4B1D").setBackground("#FF5E24").setHighlight( new
                 * NetworkNodes.Color.Highlight().setBorder("#CC4B1D").setBackground("#CC381C"))
                 * );
                 */

                NetworkNodes gamma = new NetworkNodes().setShape(Shape.icon)
                                .setIcon(new Icon().setCode("\uf1b9").setColor("#E84333")).setSize(13);

                NetworkNodes epsilon = new NetworkNodes().setShape(Shape.image)
                                .setImage("https://picsum.photos/id/" + random.nextInt(1000) + imgDimensions)
                                .setSize(30);

                NetworkNodes dseta = new NetworkNodes().setShape(Shape.image)
                                .setImage("https://picsum.photos/id/" + random.nextInt(1000) + imgDimensions)
                                .setSize(30);

                NetworkNodes eta = new NetworkNodes().setShape(Shape.image)
                                .setImage("https://picsum.photos/id/" + random.nextInt(1000) + imgDimensions)
                                .setSize(30);

                /*
                 * NetworkNodes theta = new NetworkNodes() .setShape(Shape.icon) .setIcon(new
                 * Icon().setCode("\uf102")) .setSize(15);
                 */

                groups.addGroup("alpha", alpha);
                groups.addGroup("beta", beta);
                groups.addGroup("gamma", gamma);
                groups.addGroup("delta: { shape: 'image',image:'https://picsum.photos/id/" + random.nextInt(1000)
                                + imgDimensions + "', color:'#000',mass: 5, size: 30 }");
                groups.addGroup("epsilon", epsilon);
                groups.addGroup("dseta", dseta);
                groups.addGroup("eta", eta);
                // groups.addGroup("theta", theta);

        }

        public JSONObject getDemoNetwork() {
                try {
                        String source = new String(Files.readAllBytes(Paths.get("network.json")));
                        return new JSONObject(source);
                } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        return null;
                } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                }

        }
}

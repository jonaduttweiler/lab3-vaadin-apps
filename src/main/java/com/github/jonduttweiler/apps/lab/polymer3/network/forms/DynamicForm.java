package com.github.jonduttweiler.apps.lab.polymer3.network.forms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import com.github.jonduttweiler.vaadin.components.network.Network;
import com.awesomecontrols.viswrapper.network.config.NetworkEdges;
import com.awesomecontrols.viswrapper.network.config.NetworkNodes;
import com.awesomecontrols.viswrapper.network.config.NetworkNodes.Shape;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;

import org.json.JSONObject;

public class DynamicForm extends VerticalLayout {

    List<String> reverseOrder = Arrays.asList("size","shape","width","color","title","label","group");

    Comparator<? super TextField> predefinedOrder = (a, b) -> {
        int pos1 = reverseOrder.indexOf(a.getLabel()); 
        int pos2 = reverseOrder.indexOf(b.getLabel()); 
        return pos2 - pos1;
    };
    
    
    
    
    public DynamicForm(JSONObject json) {
        
        Map<String, TextField> fields = generateFields(json);
        List<String> hiddenFields = new ArrayList<>();
        addFields(fields, hiddenFields,predefinedOrder);
        this.addClassName("dynamic-form");
    }

    public DynamicForm(JSONObject json, List<String> hiddenFields, List<String> newFields, Network network, String type) {
        Map<String, TextField> fields = generateFields(json);
        generateNewFields(json, newFields, fields);
        addFields(fields, hiddenFields,predefinedOrder);
        addBtnUpdate(network, fields,type);
        addChangeHandlers(fields,type,network);

        this.addClassName("dynamic-form");
    }

    private void addChangeHandlers(Map<String, TextField> fields, String type, Network network){
        for (TextField field : fields.values()) {
            field.addValueChangeListener(event -> {
                if(type.equals("node")){
                    NetworkNodes node = getNodeOptions(fields);
                    network.updateNode(node);
                }
    
                if(type.equals("edge")){
                    NetworkEdges edge = this.getEdgeOptions(fields);
                    network.updateEdge(edge);
                }
            });            
        }
    }

    

    private Map<String, TextField> generateFields(JSONObject json) {
        Map<String, TextField> fields = new HashMap<String, TextField>();
        Iterator<String> keys = json.keys();
        while (keys.hasNext()) {
            String key = keys.next();

            try {
                Object value = json.get(key);
                TextField field = new TextField(key);
                field.setValue(value.toString());
                fields.put(key, field);
            } catch (Exception e) {
                System.out.println("No se puede agregar: " + key);
            }
        }

        return fields;
    }

    private void addFields(Map<String, TextField> fields, List<String> hiddenFields, Comparator<? super TextField> predefinedOrder) {
        fields.values().stream()
              .filter(field -> !hiddenFields.contains(field.getLabel()))
              .sorted(predefinedOrder)
              .forEach(field -> {
                  field.addClassName("textfield-compact");
                  this.add(field);
              });

    }

    private Map<String, TextField> generateNewFields(JSONObject json, List<String> newFields,
            Map<String, TextField> fields) {

        for (int i = 0; i < newFields.size(); i++) {
            String label = newFields.get(i);
            try {
                if (json.opt(label) == null) {
                    TextField field = new TextField(label);
                    fields.put(label, field);
                }
            } catch (Exception err) {
                System.out.println(err);
            }
        }

        return fields;

    }

    private void addBtnUpdate(Network network, Map<String, TextField> fields, String type) {
        Button btn = new Button("Update");
        btn.addThemeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_SUCCESS);
        btn.addClickListener(ev -> {
            
            if(type.equals("node")){
                NetworkNodes node = getNodeOptions(fields);
                network.updateNode(node);
            }

            if(type.equals("edge")){
                NetworkEdges edge = this.getEdgeOptions(fields);
                network.updateEdge(edge);
            }
        });

        this.add(btn);
    }

    private NetworkNodes getNodeOptions(Map<String, TextField> fields) {
        Predicate<String> notEmpty = str -> !str.isEmpty();
        NetworkNodes node = new NetworkNodes();
        // comprobar que estos campos no esten vacios
        node.setId(fields.get("id").getValue());

        Optional.ofNullable(fields.get("label"))
                .map(field -> field.getValue())
                .filter(notEmpty)
                .ifPresent(x -> node.setLabel(x));

        Optional.ofNullable(fields.get("color"))
                .map(field -> field.getValue())
                .filter(notEmpty)
                .ifPresent(x -> node.setColor(x));

        Optional.ofNullable(fields.get("title"))
                .map(field -> field.getValue())
                .filter(notEmpty)
                .ifPresent(x -> node.setTitle(x));
                
        Optional.ofNullable(fields.get("size"))
                .map(field -> field.getValue())
                .filter(notEmpty)
                .map(str -> Integer.parseInt(str))
                .ifPresent(x -> node.setSize(x));

        Optional.ofNullable(fields.get("shape"))
                .map(field -> field.getValue())
                .filter(notEmpty)
                .map(x -> Shape.valueOf(x))
                .ifPresent(x -> node.setShape(x));
        
        Optional.ofNullable(fields.get("group"))
                .map(field -> field.getValue())
                .filter(notEmpty)
                .ifPresent(x -> node.setGroup(x));

        return node;
    }

    private NetworkEdges getEdgeOptions(Map<String, TextField> fields) {
        Predicate<String> notEmpty = str -> !str.isEmpty();
        NetworkEdges edge = new NetworkEdges();

        edge.setId(fields.get("id").getValue());

        Optional.ofNullable(fields.get("label")).map(field -> field.getValue()).filter(notEmpty)
                .ifPresent(x -> edge.setLabel(x));

        Optional.ofNullable(fields.get("color")).map(field -> field.getValue()).filter(notEmpty)
                .ifPresent(x -> edge.setColor(x));

        Optional.ofNullable(fields.get("arrows")).map(field -> field.getValue()).filter(notEmpty)
                .ifPresent(x -> edge.setArrows(x));

        Optional.ofNullable(fields.get("title")).map(field -> field.getValue()).filter(notEmpty)
                .ifPresent(x -> edge.setTitle(x));

        Optional.ofNullable(fields.get("width")).map(field -> field.getValue()).filter(notEmpty)
                .map(str -> Integer.parseInt(str)).ifPresent(width -> edge.setWidth(width));

        return edge;
    }
}
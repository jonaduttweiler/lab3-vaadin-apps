package com.github.jonduttweiler.apps.lab.polymer3.network.editor.panels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.github.jonduttweiler.vaadin.components.network.Network;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.textfield.TextField;

public class TravelPanel extends NetworkPanel{

    private static final long serialVersionUID = 3485708452445882212L;

    private Network network;
    private String title = "Travel panel";
    private List<String> travelPath = new ArrayList<String>(); //map?
    private Div stepsContainer;
    private int currentPos = -1;
    private Button prevBtn = new Button("◀");
    private Button nextBtn = new Button("▶");

   
    public TravelPanel(Network network){
        
        this.network = network;

        this.add(new H4(title));
        this.getStyle().set("width","100%");
        stepsContainer = new Div();
        stepsContainer.getStyle().set("width", "100%");

        travelPath = new ArrayList<String>(Arrays.asList("10","1","4","7","2","5","3"));
        
        renderList();
        this.add(stepsContainer);

        
        //div for buttons next,prev
        prevBtn.addThemeVariants(ButtonVariant.LUMO_SMALL);
        nextBtn.addThemeVariants(ButtonVariant.LUMO_SMALL);
        prevBtn.getStyle().set("color", "white").set("background-color", "#000");
        nextBtn.getStyle().set("color", "white").set("background-color", "#000");


        //div for btns container
        prevBtn.addClickListener(ev -> prev());
        nextBtn.addClickListener(ev -> next());
        
        HorizontalLayout formInlineContainer = new HorizontalLayout();
        TextField nodeIdInput = new TextField();
        nodeIdInput.setPlaceholder("NodeId");
        nodeIdInput.getStyle().set("width","150px");
        formInlineContainer.add(nodeIdInput);


        Button addBtn = new Button("Add");
        addBtn.addClickListener(ev -> {
            String nodeId = nodeIdInput.getValue();
            System.out.println("Add step:"+nodeId+"!!");
            addStep(nodeId);
            nodeIdInput.setValue("");
        });

        addBtn.getStyle().set("width","15%");
        formInlineContainer.add(addBtn);

        this.add(formInlineContainer);


        HorizontalLayout btnContainers = new HorizontalLayout(prevBtn,nextBtn);
        btnContainers.getStyle().set("width", "100%").set("justify-content","center");
        btnContainers.setAlignItems(Alignment.CENTER);

        this.add(btnContainers);

    }

    public int getCurrent(){
        return this.currentPos;
    }

  //move to prev element in travelPath.  
    public int prev(){ 
        this.currentPos = (this.currentPos == 0) ? (travelPath.size() - 1) : (this.currentPos - 1) % travelPath.size();
        setActiveStep(currentPos);
        return this.currentPos;
    }

    //move to next element in travelPath
    public int next(){
        this.currentPos = (this.currentPos + 1) % travelPath.size();
        setActiveStep(currentPos);
        return this.currentPos;
    }


    private void setActiveStep(int pos){
        String currId = travelPath.get(pos);
        stepsContainer.removeAll();
        for(String nodeId : travelPath){
            stepsContainer.add(new Div(new Text(nodeId)));
        }

        network.focus(currId);
        network.setSelection(Arrays.asList(currId),Arrays.asList(), true, false);
        
        renderList();
    }



    private void renderList(){
        stepsContainer.removeAll();
        for(int idx = 0 ; idx < travelPath.size(); idx ++){
            String nodeId = travelPath.get(idx);
            Div entryContainer = new Div(new Text(nodeId));

            if(idx == currentPos){
                entryContainer.getElement().getClassList().add("active");
            };
            stepsContainer.add(entryContainer);
        }
    }


    private void addStep(String nodeId){
        //check if exists
        System.out.println("Add step:"+nodeId);

        travelPath.add(nodeId);
        renderList();

    }




 

    

}
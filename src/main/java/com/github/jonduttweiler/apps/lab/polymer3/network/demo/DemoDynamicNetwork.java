package com.github.jonduttweiler.apps.lab.polymer3.network.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.lang.Math;

import com.awesomecontrols.viswrapper.network.config.NetworkEdges;
import com.awesomecontrols.viswrapper.network.config.NetworkNodes;

public class DemoDynamicNetwork extends DemoNetwork {

    public DemoDynamicNetwork() {
        super();
        this.stabilize();
    }

    protected void addNodes() {
        for (int i = 0; i < 40; i++) {
            String randomId = generateRandomId();
            String group = getRandomGroup();
            NetworkNodes node = new NetworkNodes(randomId, randomId, group);
            nodes.add(node);
            this.addNode(node);
        }
    }

    protected void addEdges() {
        for (int i = 0; i < 60; i++) {
            NetworkNodes from = getRandomNode();
            NetworkNodes to = getRandomNode();
            if(from.getId().equals(to.getId())) {
                i--;
                return;
            }

            String randomColor = getRandomColor();
            int randomWidth = (1 + (int) Math.floor(Math.random()*3) )* 2;


            NetworkEdges networkEdge = new NetworkEdges()
                                          .setFrom(from.getId())
                                          .setTo(to.getId())
                                          .setColor(randomColor)
                                          .setWidth(randomWidth);
            this.addEdge(networkEdge);
        }
    }

    private String generateRandomId() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97)).limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();

        return generatedString;
    }

    private NetworkNodes getRandomNode() {
        int nnodes = nodes.size();
        int randomIdx = (int) Math.floor(Math.random() * nnodes);
        return nodes.get(randomIdx);
    }

    private String getRandomGroup() {
        int ngroups = groupNames.size();
        int randomIdx = (int) Math.floor(Math.random() * ngroups);
        return groupNames.get(randomIdx);
    }

    private String getRandomColor() {
        List<String> colors = Arrays.asList("#FF6440","#B5E2E8","#B5E2E8","#99C47E","#32C4EB");
        int ncolors = colors.size();
        int randomIdx = (int) Math.floor(Math.random() * ncolors);
        return colors.get(randomIdx);
    }


}
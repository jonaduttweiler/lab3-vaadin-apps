package com.github.jonduttweiler;


import com.github.jonduttweiler.apps.lab.polymer3.network.demo.DemoNetwork;
import com.github.jonduttweiler.vaadin.components.network.Network;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;

/**
 * The main view contains a button and a click listener.
 */
@Route("")
@PWA(name = "Vaadin integration App Polymer3", shortName = "Lab3")
//TODO: FIX THIS, include FA in the component
@StyleSheet("https://use.fontawesome.com/releases/v5.12.1/css/all.css")
public class MainView extends VerticalLayout {

    Network network;

    public MainView() {

        this.getStyle().set("height", "100vh");
        this.network = new DemoNetwork();
        network.stabilize();
        
        add(network);
      
    }

}
